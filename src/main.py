import re
from typing import Generator

from pypdf import PdfReader

from src.schemas import Point


def pdf_pages_text(file_path: str) -> list[str]:
    with open(file_path, 'rb') as file:
        pdf = PdfReader(file)
        for page in pdf.pages:
            yield page.extract_text()


def parse_points_from_page(page_text: str) -> Generator:
    matches = re.finditer(
        r"(?P<location>.+)\s(?P<latitude>\d{6})(?P<latitude_hemisphere>N|S) "
        r"\s?(?P<longitude>\d{7})(?P<longitude_hemisphere>W|E) (?P<code>\w{2,5}) (?P<sector>.{2,7})",
        page_text)
    for match in matches:
        yield Point(**match.groupdict())


def parse_points(file_path: str):
    points = list()
    for page in pdf_pages_text(file_path):
        points.extend([point for point in parse_points_from_page(page)])
    return points


def pdf_to_gpx(file_path: str) -> str:
    output = '<?xml version="1.0" encoding="UTF-8"?>\n' \
             '<gpx version="1.0">\n'
    for point in parse_points(file_path):
        output += point.export_to_gpx()
    output += '</gpx>'
    return output
