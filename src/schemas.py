from pydantic import BaseModel
from unidecode import unidecode

from src.helpers import dms_to_dd


class Point(BaseModel):
    location: str
    latitude: str
    latitude_hemisphere: str
    longitude: str
    longitude_hemisphere: str
    code: str
    sector: str

    @property
    def lat(self) -> float:
        return dms_to_dd(degrees=f"{self.latitude[:2]}", minutes=f"{self.latitude[2:4]}",
                         seconds=f"{self.latitude[4:]}")

    @property
    def lon(self) -> float:
        return dms_to_dd(degrees=f"{self.longitude[:3]}", minutes=f"{self.longitude[3:5]}",
                         seconds=f"{self.longitude[5:]}")

    def export_to_gpx(self):
        return f'\t<wpt lat="{"-" if self.latitude_hemisphere == "S" else ""}{self.lat}" lon="{"-" if self.longitude_hemisphere == "W" else ""}{self.lon}">\n' \
               f'\t\t<name>{unidecode(self.location)}</name>\n' \
               f'\t\t<sym>WhiteDot</sym>\n' \
               f'\t\t<extensions>\n' \
               f'\t\t\t<identifier>{self.code}</identifier>\n' \
               f'\t\t</extensions>\n' \
               f'\t</wpt>\n'
