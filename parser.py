from src.main import pdf_to_gpx


def run():
    with open('output.gpx', 'w') as file:
        file.truncate(0)
        file.write(pdf_to_gpx("localidades.pdf"))


if __name__ == '__main__':
    run()
