# Localidades GPX

### Objective

Have the VFR points in Mapping Software

### [Downloads](https://gitlab.com/rodrigo.f.simoes/localidades-gpx/-/snippets)

### Importing 
#### SkyDeamon
![](images/skydeamon-example.png)

1. Open Waypoints Menu
2. Create `New Folder`
3. Rename the created folder
4. Import `output.gpx` file
5. `Sync with Cloud`

![](images/skydeamonimport.png)



Now on all other devices you only have to click on `Sync with Cloud` button

##### Mobile
1. Open menu: Upper left corner cogwheel
2. `User Waypoints`
3. `Sync with Cloud`

#### Google Earth
![](images/googleearth-example.png)

1. On the toolbar select `File`
2. Click button `Open...`

![](images/googleearth-openfile.png)

(Will open file explorer window)

3. Open file extension selection box
4. Select `Gps`
5. Browser for your `output.gpx` file
6. Open file

![](images/googleearth-openfileextension.png)

(Will close file explorer window and open google earth dialog)

7. Click ok

![](images/googleearth-importok.png)


### Using the script to generate points

#### Expected input
PDF File with data in format ``LOCALIDADE | COORDENADAS | CÓDIGO 5 LETRAS | SECTOR``

#### Procedure
1. Python Installed > 3.9
2. Python Virtual Env Initialized
3. Install dependencies: `pip install -r .\requirements.txt`
4. Copy pdf file to the root directory and rename as `localidades.pdf`
5. Run script: `python parser.py`
6. Use output that will be generated as `output.gpx`
